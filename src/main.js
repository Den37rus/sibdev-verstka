class MenuListener {
  init() {
    this.$sidebar = document.querySelector('.sidebar');
    this.$overlay = document.querySelector('.menu-overlay');
    this.$nav = document.querySelector('.menu-list');
    this.$menuBtn = document.querySelector('.menu-button');
    this.addListeners();
  }

  addListeners() {
    this.$nav.addEventListener('click', this.navigate.bind(this));
    this.$menuBtn.addEventListener('click', this.toggleMenu.bind(this));
    this.$overlay.addEventListener('click', this.toggleMenu.bind(this));
  }

  toggleMenu() {
    document.body.classList.toggle('no-scroll');
    this.$menuBtn.classList.toggle('menu-button__active');
    this.$overlay.classList.toggle('menu-overlay__active');
    this.$sidebar.classList.toggle('menu-open');
  }

  hideMenu() {
    document.body.classList.remove('no-scroll');
    this.$menuBtn.classList.remove('menu-button__active');
    this.$overlay.classList.remove('menu-overlay__active');
    this.$sidebar.classList.remove('menu-open');
  }

  navigate(e) {
    const menuEl = e.target.closest('li');
    if (!menuEl) return;
    Array.from(this.$nav.children).forEach((el) => el.classList.remove('menu-list--item__active'));
    menuEl.classList.add('menu-list--item__active');
    document.querySelector('.header-title').textContent = menuEl.textContent.trim();
    this.hideMenu();
  }
}

class CardElement {
  constructor(card = {}) {
    this.template = `<div class="card" style="background-image: url(${card.image})">
      <div class="card-content" >
        <div class="content-title">
          <h3 class="title-title">${card.titleText}</h3>
          <p class="title-description">${card.titleDescription}</p>
        </div>
        <div class="content-discount">
          <h2 class="discount-content">${card.discountValue}</h2>
          <p class="discount-description">${card.discountDescription}</p>
        </div>
      </div>
    </div>`;
    const cardWraper = document.createElement('div');
    cardWraper.className = 'card-wrapper';
    cardWraper.insertAdjacentHTML('beforeend', this.template);
    return cardWraper;
  }

  static createList(cardsList) {
    const fragment = document.createDocumentFragment();
    cardsList.forEach((card) => {
      const cardEl = new CardElement(card);
      fragment.append(cardEl);
    });
    return fragment;
  }
}

const searchHandler = () => {
  const shop = document.querySelector('.header-input')?.value;
  if (shop) {
    alert(`Поиск магазина ${shop}`);
  }
};

window.onload = () => {
  const menuListener = new MenuListener();
  menuListener.init();

  const cardsFragment = CardElement.createList(cards);
  document.querySelector('main').append(cardsFragment);

  document.querySelector('.header-input--label').addEventListener('click', searchHandler);

  const el = document.querySelector('header');
  const observer = new IntersectionObserver(([e]) => e.target.classList.toggle('is-pinned', e.intersectionRatio < 1), {
    threshold: [1],
  });
  observer.observe(el);
};
