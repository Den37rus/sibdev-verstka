const gulp = require('gulp'),
  browserSync = require('browser-sync').create(),
  sass = require('gulp-sass')(require('sass')),
  prefixer = require('gulp-autoprefixer'),
  cssmin = require('gulp-clean-css'),
  uglify = require('gulp-uglify'),
  fileinclude = require('gulp-file-include'),
  gcmq = require('gulp-group-css-media-queries'),
  rename = require('gulp-rename'),
  babel = require('gulp-babel'),
  include = require('gulp-include'),
  concat = require('gulp-concat');

const outputPath = 'dist/';

gulp.task('html_build', function () {
  return gulp.src('src/*.html').pipe(fileinclude()).pipe(gulp.dest(outputPath)).pipe(browserSync.stream());
});

gulp.task('copy', function () {
  return gulp.src('src/img/*.*').pipe(gulp.dest('dist/img/'));
});

gulp.task('css_build', function () {
  return gulp
    .src('src/*.scss')
    .pipe(sass())
    .pipe(prefixer())
    .pipe(gcmq())
    .pipe(cssmin())
    .pipe(rename({ extname: '.min.css' }))
    .pipe(include())
    .on('error', console.log)
    .pipe(gulp.dest(outputPath))
    .pipe(browserSync.stream());
});

gulp.task('js_build', function () {
  return gulp
    .src('src/*.js')
    .pipe(concat('main.js'))
    .pipe(uglify())
    .pipe(rename({ extname: '.min.js' }))
    .pipe(include())
    .on('error', console.log)
    .pipe(gulp.dest(outputPath))
    .pipe(browserSync.stream());
});

gulp.task('webServer', function (done) {
  browserSync.init({
    server: outputPath,
  });
  gulp.watch('src/**/*.html', gulp.series('html_build'));
  gulp.watch('src/**/*.scss', gulp.series('css_build'));
  gulp.watch('src/**/*.js', gulp.series('js_build'));
  done();
});

gulp.task('default', gulp.series('css_build', 'js_build', 'webServer', 'copy', 'html_build'));
